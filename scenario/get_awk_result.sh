#!/bin/bash

# declare input and variable
PATH=$(pwd)
TR_FILENAME=$1
WORKPATH=$2
TR_FILE=$WORKPATH/$TR_FILENAME
if [ -f $PATH/awk/pdr.awk ]; then
  /bin/cp $PATH/awk/*.awk $WORKPATH
else
  /bin/cp $PATH/../scenario/awk/*.awk $WORKPATH
fi



# run awk and save to txt
/usr/bin/awk -f $WORKPATH/dropped.awk $TR_FILE > $WORKPATH/dropped.txt
/usr/bin/awk -f $WORKPATH/e2e.awk $TR_FILE > $WORKPATH/e2e.txt
/usr/bin/awk -f $WORKPATH/gp.awk $TR_FILE > $WORKPATH/gp.txt
/usr/bin/awk -f $WORKPATH/ifq.awk $TR_FILE > $WORKPATH/ifq.txt
/usr/bin/awk -f $WORKPATH/pdr.awk $TR_FILE > $WORKPATH/pdr.txt
# /usr/bin/awk -f $WORKPATH/route_check.awk $TR_FILE > $WORKPATH/route_check.txt
/usr/bin/awk -f $WORKPATH/snippet.awk $TR_FILE > $WORKPATH/snippet.txt

# print result
printf "\nshowing result of tr file $TR_FILENAME on $WORKPATH\n"

printf "\ndropped packet\n"
/bin/cat $WORKPATH/dropped.txt

printf "\nend to end delay\n"
/bin/cat $WORKPATH/e2e.txt

printf "\npacket type\n"
/bin/cat $WORKPATH/gp.txt

printf "\nifq packet\n"
/bin/cat $WORKPATH/ifq.txt

printf "\nPacket delivery ratio\n"
/bin/cat $WORKPATH/pdr.txt

# printf "\nroute check\n"
# /bin/cat $WORKPATH/route_check.txt

# printf "\nsnippet packet\n"
# /bin/cat $WORKPATH/snippet.txt

# delete awk and txt file
/bin/rm $WORKPATH/*.awk
# /bin/rm $WORKPATH/dropped.txt $WORKPATH/e2e.txt $WORKPATH/gp.txt $WORKPATH/ifq.txt $WORKPATH/pdr.txt $WORKPATH/route_check.txt $WORKPATH/snippet.txt
/bin/rm $WORKPATH/dropped.txt $WORKPATH/e2e.txt $WORKPATH/gp.txt $WORKPATH/ifq.txt $WORKPATH/pdr.txt
