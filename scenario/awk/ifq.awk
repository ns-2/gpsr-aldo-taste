# AWK Script for Packet Delivery Calculation for OLD Trace Format

BEGIN {
IFQ=0;
}

$0 ~/^D.* IFQ.* /{
	IFQ++;
}

END {
 printf "ifq:%d\n",IFQ;

}
