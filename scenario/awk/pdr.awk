# AWK Script for Packet Delivery Calculation for OLD Trace Format

BEGIN {
sent=0;
received=0;
forward=0;
dropped= 0;
}

$0 ~/^s.* AGT/{
	sent++;
}

$0 ~/^r.* AGT/{
	received++;
}

$0 ~/^f.* RTR.* cbr/{
	forward++;
}

$0 ~/^D.* RTR.* cbr/{
	dropped++;
}


END {

 printf " s:%d",sent;
 printf "\n r:%d",received;
 printf "\n f:%d",forward;
printf "\n D:%d\n",dropped;
 printf "%.2f\n",(received/sent)*100;

}
