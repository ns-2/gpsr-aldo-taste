BEGIN {
	greedy = 0;
	perimeter = 0;
	flag = 0;
	flag2=-100;
	a=0;
	b=0;
}

{
	event = $1;
	time = $2;
	type = $18;
	packet_type = $7;

	if ($1 == "r" || $1 == "s" || $1 == "f" || $1 == "D"){ 
		if (packet_type == "cbr"){
			flag = 1;
		}
		if (type == "[0]"){
			flag2=0;
			a++;
		}
		if (type == "[1]"){
			flag2=1;
			b++;
		}
	}

	if ($1 == "r" || $1 == "s" || $1 == "f" || $1 == "D"){ 
		if (packet_type != "cbr"){
			flag = 0;
			flag2=-100;
		}
	}

	if ($1 ~ /HLS/){
		flag = 0;
		flag2=-100;
	}

	if (flag == 1 && event == "VFP"){
		if (flag2==0) greedy++;
		if (flag2==1) perimeter++;
	}

	# if (flag == 1 && event == "VFP"){
	# 	if (type == "[0]") greedy++;
	# 	if (type == "[1]") perimeter++;
	# }
}

END {
	printf ("g: %d, p: %d\n", a, b);
	printf ("g: %d, p: %d\n", greedy, perimeter);
	printf ("%.2f\n", greedy/perimeter);
}