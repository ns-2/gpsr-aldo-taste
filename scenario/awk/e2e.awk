BEGIN {
highest_packet_id = 0;
}
{
  action = $1;
  time = $2;
  layer = $4;
  packet_id = $6;
  packet_type[packet_id] = $7;

  # Check for the latest packet id
  if ($1 == "r" || $1 == "s" || $1 == "f" || $1 == "D"){ 
  if ( packet_id > highest_packet_id ) {
    highest_packet_id = packet_id;
  }
  # If start_time of packet_id is empty, then get read time
  if ( start_time[packet_id] == 0 ) {
    start_time[packet_id] = time;
  }

  # If packet is not dropped
  if ( action != "d" ) {
    # If packet is received
    if ( action == "r" ) {
      # Get received time
      end_time[packet_id] = time;
    }
  }
  # If packet is dropped
  else {
    # There is no "end time"
    end_time[packet_id] = -1;
  }
}
}
END {
  sigma_duration = 0;
  count = 0;
  for ( packet_id = 0; packet_id <= highest_packet_id; packet_id++ )
  {
    type = packet_type[packet_id];
    start = start_time[packet_id];
    end = end_time[packet_id];
    packet_duration = end - start;
    if ( start < end ) {
      if ( packet_type[packet_id] == "cbr" ) {
          sigma_duration += packet_duration;
          count++;
      }
      # printf("%d %s: %f %f\n", packet_id, type, start, packet_duration);
    }
  }
  if ( count == 0 ) {
    printf("no_packet_counted\n");
  }
  else {
    printf("%.2f\n", sigma_duration / count);
  }

}
