# AWK Script for Packet Delivery Calculation for OLD Trace Format

BEGIN {
NRTE=0;
TTL=0;
SBF=0;
CBK= 0;
}

$0 ~/^D.* RTR.* NRTE.* cbr/{
	NRTE++;
}

$0 ~/^D.* RTR.* TTL.* cbr/{
	TTL++;
}

$0 ~/^D.* RTR.* SBF.* cbr/{
	SBF++;
}

$0 ~/^D.* RTR.* CBK.* cbr/{
	CBK++;
}


END {

 printf " nrte:%d",NRTE;
 printf "\n ttl:%d",TTL;
 printf "\n sbf:%d",SBF;
 printf "\n cbk:%d\n",CBK;

}
