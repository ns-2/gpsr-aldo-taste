#!/bin/bash

# declare input and variable
nn=$1
nn_gen=$(/usr/bin/expr $nn - 2)
PATH=$(pwd)
WORKPATH=$PATH/../simulation/$nn-real

# make folder
if [ -d $WORKPATH ]; then
  /bin/rm -rf $WORKPATH
fi
/bin/mkdir -p $WORKPATH

# create real scenario
/bin/sh create_real_scenario.sh $nn_gen $WORKPATH

# enter folder
cd $WORKPATH

# create script
/bin/cp $PATH/template_script.tcl $WORKPATH/script.tcl
/bin/sed -i 's/X_PLACEHOLDER/1645/g' $WORKPATH/script.tcl;
/bin/sed -i 's/Y_PLACEHOLDER/1690/g' $WORKPATH/script.tcl;
/bin/sed -i 's/IFQ_PLACEHOLDER/512/g' $WORKPATH/script.tcl;
/bin/sed -i 's/SEED_PLACEHOLDER/1.0/g' $WORKPATH/script.tcl;
/bin/sed -i 's/PROTOCOL_PLACEHOLDER/GPSR/g' $WORKPATH/script.tcl;
/bin/sed -i 's/NODE_PLACEHOLDER/'$nn_gen'/g' $WORKPATH/script.tcl;
/bin/sed -i 's/NALL_PLACEHOLDER/'$nn'/g' $WORKPATH/script.tcl;
/bin/sed -i 's/START_PLACEHOLDER/40/g' $WORKPATH/script.tcl;
/bin/sed -i 's/STOP_PLACEHOLDER/240/g' $WORKPATH/script.tcl;
/bin/sed -i 's/OUTPUT_PLACEHOLDER/gpsr_tracefile.tr/g' $WORKPATH/script.tcl;
/bin/sed -i 's/NAM_PLACEHOLDER/gpsr_tracefile.nam/g' $WORKPATH/script.tcl;
/bin/sed -i 's/RATE_PLACEHOLDER/2kB/g' $WORKPATH/script.tcl;

# run
/usr/local/bin/ns-hls-mod $WORKPATH/script.tcl
cd ..

# get result
/bin/sh $PATH/get_pl_result.sh gpsr_tracefile.tr $WORKPATH
/bin/sh $PATH/get_awk_result.sh gpsr_tracefile.tr $WORKPATH

# delete trace and nam file
# /bin/rm $WORKPATH/*.tr
/bin/rm $WORKPATH/*.nam
