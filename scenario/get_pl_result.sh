#!/bin/bash

# declare input and variable
PATH=$(pwd)
TR_FILENAME=$1
WORKPATH=$2
TR_FILE=$WORKPATH/$TR_FILENAME
if [ -f $PATH/perl/hls_evaluate.pl ]; then
  /bin/cp $PATH/perl/*.pl $WORKPATH
else
  /bin/cp $PATH/../scenario/perl/*.pl $WORKPATH
fi


# run perl and save to txt
/usr/bin/perl $WORKPATH/hls_evaluate.pl -f $TR_FILE > $WORKPATH/hls.txt
/usr/bin/perl $WORKPATH/gls_evaluate.pl -f $TR_FILE > $WORKPATH/gls.txt

# print result
printf "\nshowing result of tr file $TR_FILENAME on $WORKPATH\n"

printf "\nhls version\n"
/bin/cat $WORKPATH/hls.txt

printf "\ngls version\n"
/bin/cat $WORKPATH/gls.txt

# delete pl and txt file
/bin/rm $WORKPATH/hls_evaluate.pl $WORKPATH/gls_evaluate.pl
/bin/rm $WORKPATH/hls.txt $WORKPATH/gls.txt
