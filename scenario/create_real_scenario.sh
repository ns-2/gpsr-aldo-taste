#!/bin/bash

# declare input and variable
nn=$1
path=$2
# nn_ori=$(/usr/bin/expr $nn + 2)
# path=$(pwd)/$nn_ori
SEED=1 #was $RANDOM

# export SUMO_HOME (needed)
SUMO_HOME=/usr/share/sumo
export SUMO_HOME=$SUMO_HOME

# print status
printf "building scenario node=$nn real\n"

# convert osm file to xml
/usr/bin/netconvert --osm-files ./src/map-aldo.osm --output-file=$path/map.net.xml

# create random trips
/usr/bin/python /usr/share/sumo/tools/randomTrips.py -n $path/map.net.xml -e $nn --intermediate=500 -l --seed $SEED --trip-attributes="departLane=\"best\" departSpeed=\"max\" departPos=\"random_free\"" --output-trip-file=$path/trips.trips.xml
if [ -f $path/trips.trips.xml ]; then
  printf "\n"
else
  /bin/mv $(pwd)/trips.trips.xml $path/trips.trips.xml
fi

# create route for trips and scenario
/usr/bin/duarouter -n $path/map.net.xml -t $path/trips.trips.xml -o $path/route.rou.xml --ignore-errors --repair

# make a rule time start and stop
/bin/cp ./src/map.sumo.cfg.xml $path/map.sumo.cfg.xml
/bin/sed -i 's/200/240/g' $path/map.sumo.cfg.xml
/usr/bin/sumo -c $path/map.sumo.cfg.xml --fcd-output $path/scenario.xml

# export to activity and mobility file
/usr/bin/python /usr/share/sumo/tools/traceExporter.py --fcd-input=$path/scenario.xml --ns2config-output $path/map.config.tcl --ns2activity-output $path/map.activity.tcl --ns2mobility-output $path/map.mobility.tcl
/bin/sed -i 's/stop/reset/g' $path/map.activity.tcl
/bin/sed -i 's/$g/$node_/g' $path/map.activity.tcl
/bin/sed -i 's/.*-.*/#/' $path/map.mobility.tcl
/bin/sed -i '/setdest [0-9\.]* 0\.0 /s/^/#/' $path/map.mobility.tcl
/bin/sed -i '/setdest 0\.0 [0-9\.]* /s/^/#/' $path/map.mobility.tcl
/bin/sed -i '/setdest [0-9\.]* [0-9\.]* 0\.0/s/^/#/' $path/map.mobility.tcl

# remove unused file
if [ -f $path/map.net.xml ]; then
  /bin/rm $path/map.net.xml
fi
if [ -f $path/route.rou.alt.xml ]; then
  /bin/rm $path/route.rou.alt.xml
fi
if [ -f $path/route.rou.xml ]; then
  /bin/rm $path/route.rou.xml
fi
if [ -f $path/trips.trips.xml ]; then
  /bin/rm $path/trips.trips.xml
fi
if [ -f $path/scenario.xml ]; then
  /bin/rm $path/scenario.xml
fi
if [ -f $path/map.sumo.cfg.xml ]; then
  /bin/rm $path/map.sumo.cfg.xml
fi
