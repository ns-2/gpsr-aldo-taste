# HOLA
## guide to use
### 1. clone this folder to ns2.35 folder (already patched with gpsr-hls)
### 2. remove cc and h file, and copy gpsr hls ori
```
$ cd {path-to-ns2}/ns-2.35/gpsr
$ rm gpsr.cc gpsr.h
$ mv gpsr.h.ori gpsr.h
$ mv gpsr.cc.ori gpsr.cc
```
### 3. compile ns2.35
```
$ cd {path-to-ns2}/ns-2.35/
$ make clean
$ make
$ make install
$ cp ns /usr/local/bin/ns-hls-mod
```
### 4. run simulation in grid map
```
$ cd {path-to-ns2}/ns-2.35/gpsr/scenario
$ sudo su
$ ./single_grid_run.sh {number_of_node} (ex: ./single_grid_run.sh 40)
```
folder result on {path-to-ns2}/ns-2.35/gpsr/simultion/{number_of_node}-grid
### 5. run simulation in real map
```
$ cd {path-to-ns2}/ns-2.35/gpsr/scenario
$ sudo su
$ ./single_real_run.sh {number_of_node} (ex: ./single_real_run.sh 40)
```
folder result on {path-to-ns2}/ns-2.35/gpsr/simultion/{number_of_node}-real

## configuration
### 1. how to change map.osm for real simulation?
```
Map.osm file placed on cd {path-to-ns2}/ns-2.35/gpsr/scenario/src, delete map osm old file and replace with new map osm file
```
